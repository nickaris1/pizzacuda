
#include "device_launch_parameters.h"
#include "cuda_runtime.h"
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <sstream> 
#include <string>
#include <vector>

#define OOF 300
#define DEBUG
//#undef DEBUG



__global__ void zero_array(int** dp, int size) {
    int i = threadIdx.x;
    if (i < size) {
        dp[0][i] = 0;
        dp[1][i] = 0;
    }
}

__global__ void dup_row(int** dp, size_t size) {
    int i = threadIdx.x;
    if (i < size) {
        dp[0][i] = dp[1][i];
    }
}

__global__ void par_set(int** dp, int wt) {
    int w = threadIdx.x + blockIdx.x * blockDim.x;
    if (w > 0) {
         if(wt>w){
             dp[1][w] = dp[0][w];
         } else {
             dp[1][w] = max(dp[0][w], dp[0][w - wt] + wt);
#ifdef DEBUG
             printf("dp[1][%d] = %d\n", w, dp[1][w]);
#endif
         }
    }
}

void print_dp(int** dp, size_t size) {
    for (size_t i = 0; i < size; i++) {
        std::cout << dp[0][i] << " ";
    }
    std::cout << std::endl;
    for (size_t i = 0; i < size; i++) {
        std::cout << dp[1][i] << " ";
    }
    std::cout << std::endl;
}

std::vector<int> knap(std::vector<int> items, int T) {
    unsigned int dp_size = T + 1;
    cudaError_t err;

    unsigned int blocksize = 1024;
    unsigned int nblocks = (dp_size / blocksize) + 1;
    if (blocksize > dp_size) {
        std::cout << "[+]blocksize > dp_size\n";
        blocksize = dp_size;
    }
    std::cout << "bloksize " << blocksize << " " << "||blocks " << nblocks << " ||dp_size " << dp_size<< std::endl;

    err = cudaGetLastError();
    if (err != cudaSuccess)
        printf("Error: %s\n", cudaGetErrorString(err));
    
#ifdef DEBUG
    printf("StartMemoryAlloc\n");
#endif

    int** dp;
    cudaMallocHost(&dp, 2 * sizeof(int*));
    cudaMallocHost(&(dp[0]), (dp_size) * sizeof(int));
    cudaMallocHost(&(dp[1]), (dp_size) * sizeof(int));

    /*int** dp;
    cudaMallocManaged(&dp, 2 * sizeof(int*));
    cudaMallocManaged(&(dp[0]), (dp_size) * sizeof(int));
    cudaMallocManaged(&(dp[1]), (dp_size) * sizeof(int));*/
    err = cudaGetLastError();
    if (err != cudaSuccess)
        printf("Error: %s\n", cudaGetErrorString(err));
#ifdef DEBUG
    printf("EndMemoryAlloc\n");
#endif

    std::vector<int> results;
    std::vector<int> bool_r(items.size(), 0);

    for (size_t i = items.size(); i > 0; i--) {
#ifdef DEBUG
        std::cout << "[DEBUG] zero arr" << dp_size << std::endl;
#endif

        err = cudaGetLastError();
        if (err != cudaSuccess)
            printf("[+]Error: %s\n", cudaGetErrorString(err));

        zero_array <<<nblocks, blocksize>>> (dp, dp_size);
        cudaDeviceSynchronize();

        err = cudaGetLastError();
        if (err != cudaSuccess)
            printf("Error: %s\n", cudaGetErrorString(err));
#ifdef DEBUG
        std::cout << "[DEBUG] zero arr end\n";
#endif
        
        for (int index = 1; index < i + 1; index++) {
#ifdef DEBUG
            std::cout << "[DEBUG] dup arr\n";
#endif
            dup_row <<<nblocks, blocksize>>> (dp, dp_size);
            cudaDeviceSynchronize();

            err = cudaGetLastError();
            if (err != cudaSuccess)
                printf("Error: %s\n", cudaGetErrorString(err));
#ifdef DEBUG
            std::cout << "[DEBUG] dup arr end\n";
#endif
            err = cudaGetLastError();
            if (err != cudaSuccess)
                printf("Error: %s\n", cudaGetErrorString(err));
            
            int wt = items[index - 1];
            
            par_set <<<nblocks, blocksize >>> (dp, wt);
            cudaDeviceSynchronize();
            err = cudaGetLastError();
            if (err != cudaSuccess) {
                printf("Error: %s\n", cudaGetErrorString(err));
                exit(1);
            }
        }
        if (dp[1][T] != dp[0][T]) {
            bool_r[i - 1] = 1;
            results.push_back(items[i - 1]);
            T -= items[i - 1];
        }

    }

    cudaFree(&dp[0]);
    cudaFree(&dp[1]);
    cudaFree(&dp);
    return bool_r;
}


int main (int argc, char* argv[]) {
    struct cudaDeviceProp properties;
    cudaGetDeviceProperties(&properties, 0);
    std::cout << "using " << properties.multiProcessorCount << " multiprocessors" << std::endl;
    std::cout << "max threads per processor: " << properties.maxThreadsPerMultiProcessor << std::endl;
    //std::cout << argv[0] << argv[1] << std::endl;

    std::string line;
    int T, P;
    std::vector<int> pizzas;

    std::ifstream myfile(argv[1], std::ios_base::in);
    if (myfile.is_open()){
        
        myfile >> T >> P;
        std::cout << T << " " << P << std::endl;
        for (int i = 0; i < P; i++) {
            pizzas.push_back(0);
        }
        for (int i = 0; i < P; i++) {
            myfile >> pizzas[i];
        }
        myfile.close();
    }
    for (int i = 0; i < pizzas.size(); i++) {std::cout << pizzas[i] << " ";}
    std::cout << std::endl;

    std::vector<int> r = knap(pizzas, T);
    for (int i = 0; i < r.size(); i++) {std::cout << r[i] << " ";}
    std::cout << std::endl;
    return 0;
}